package br.com.teste.spring;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.teste.spring.entity.Cliente;
import br.com.teste.spring.repository.ClienteRepository;
import br.com.teste.spring.repository.search.ClienteSpecification;
import br.com.teste.spring.repository.search.SearchCriteria;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SpringDataApplicationTests {
	
	@Autowired
    private MockMvc mockMvc;

	@Autowired
	private ClienteRepository repository;
	
    private Cliente cliente1;
    private Cliente cliente2;

//	@Test
//	public void caso1() throws Exception {
//		
//		mockMvc.perform(post("/clientes/1")
//			   .contentType("application/json"))
//			   .andExpect(status().isOk());
//	}
	
	@Test
	public void caso2() throws Exception {
		
		mockMvc.perform(get("/clientes")
			   .contentType("application/json"))
			   .andExpect(status().isOk())
			   ;
			   //.andExpect(jsonPath("$", hasSize(4)))
			   //.andExpect(jsonPath("$[1].nome", is("Rodrigo")));
		
	}

}
