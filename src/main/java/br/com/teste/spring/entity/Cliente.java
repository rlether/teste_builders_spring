package br.com.teste.spring.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;
import org.joda.time.Period;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "Cliente")
@Table(name = "CLIENTE")
@ToString
@EqualsAndHashCode
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Sequencial 
	 */
	@Id
	@Getter
	@Setter
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID_CLIENTE", nullable = false, precision = 10, scale = 0)
	private Long id;

	/**
	 * Documento: CPF / CNPJ
	 */
	@Getter
	@Setter
	@NotNull
	@Digits(integer = 15, fraction = 0)
	@Column(name = "DOCUMENTO", nullable = false, precision = 15, scale = 0)
	private String documento;

	/**
	 * Nome.
	 */
	@Getter
	@Setter
	@Size(min = 0, max = 50)
	@Column(name = "NOME", nullable = false, length = 50)
	private String nome;
	
	/**
	 * Nome Mae.
	 */
	@Getter
	@Setter
	@Size(min = 0, max = 50)
	@Column(name = "NOME_MAE", nullable = false, length = 50)
	private String nomeMae;

	/**
	 * Nome Pai.
	 */
	@Getter
	@Setter
	@Size(min = 0, max = 50)
	@Column(name = "NOME_PAI", length = 50)
	private String nomePai;
	
	/**
	 * Data de Nascimento.
	 */
	@Getter
	@Setter
	@JsonFormat (shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Column(name = "DT_NASC")
	private Date dt_nascimento;
	
	@Setter
	@Transient
	private Integer idade;
	
	public Integer getIdade() {
		
		if (this.dt_nascimento != null) {
			
			DateTime d1 = new DateTime(getDt_nascimento());
			DateTime d2 = new DateTime(new Date());
			Period period = new Period(d1, d2);
		
			return period.getYears();
			
		} else {
			return 0;
		}
	}
	
}