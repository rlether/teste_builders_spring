package br.com.teste.spring.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class CustomErrorController implements ErrorController  {
	
	@RequestMapping("/error")
	public ModelAndView handleError(HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView();
		
		 int status = response.getStatus();
		 switch (status) {
		 	case 400: // HttpStatus.BAD_REQUEST
		 		modelAndView.setViewName("error-400");
		 		break;
		 	case 403: // HttpStatus.FORBIDDEN
		 		modelAndView.setViewName("error-403");
		 		break;
		 	case 404: // HttpStatus.NOT_FOUND
		 		modelAndView.setViewName("error-404");
		 		break;
		 	case 500: // HttpStatus.INTERNAL_SERVER_ERROR
		 		modelAndView.setViewName("error-500");
		 		break;
		 	 default:
		 		modelAndView.setViewName("error");
		 }
		 return modelAndView;
	}
	
	@Override
	public String getErrorPath() {
		return "/error";
	}
}
