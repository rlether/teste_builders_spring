package br.com.teste.spring.config;

public class ClienteNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ClienteNotFoundException(Long id) {
		super("Nao foi possivel encontrar Entity Pessoa : " + id);
	}
	  
}
