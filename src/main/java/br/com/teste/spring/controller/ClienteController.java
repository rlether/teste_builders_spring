package br.com.teste.spring.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.teste.spring.config.ClienteNotFoundException;
import br.com.teste.spring.entity.Cliente;
import br.com.teste.spring.repository.ClienteRepository;
import br.com.teste.spring.repository.search.ClienteSpecificationsBuilder;

@RestController
@RequestMapping(path = "/clientes")
@Validated
public class ClienteController {

	@Autowired
	private ClienteRepository repository;
	
	@GetMapping
	public ResponseEntity<Map<String, Object>> listPaginated(
			@RequestParam(value = "page", required = false, defaultValue = "0") int page, 
			@RequestParam(value = "limit", required = false, defaultValue = "4") int limit,
			@RequestParam(value = "sort", required = false, defaultValue = "id") String sort,
			@RequestParam(value = "search", required = false, defaultValue = "") String search)
	{
		try {
			
	        ClienteSpecificationsBuilder builder = new ClienteSpecificationsBuilder();
	        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
	        Matcher matcher = pattern.matcher(search + ",");
	        while (matcher.find()) {
	            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
	        }
	        
	        Specification<Cliente> spec = builder.build();
			
			Pageable paging = PageRequest.of(page, limit, Sort.Direction.ASC, sort);
			
			Page<Cliente> pageClientes = repository.findAll(spec, paging);
			
			List<Cliente> listaClientes = pageClientes.getContent();
			
			Map<String, Object> response = new HashMap<>();
		    response.put("pessoas", listaClientes);
		    response.put("currentPage", pageClientes.getNumber());
		    response.put("totalItems", pageClientes.getTotalElements());
		    response.put("totalPages", pageClientes.getTotalPages());
			
		    return new ResponseEntity<>(response, HttpStatus.OK);
		    
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
    @GetMapping(value = "/{id}")
    public Cliente findById(@PathVariable @Min(1) Long id) {
    	
    	Optional<Cliente> retorno = repository.findById(id);
    	
    	if (retorno.isPresent()) {
    		
	    	return retorno.get();
    	
    	} else {
    		throw new ClienteNotFoundException(id);
    	}
    }
    
    @PostMapping
    public ResponseEntity<?> create(@RequestBody Cliente resource) {
    	
    	try {
    		
    		Cliente response = repository.save(resource);
    		
    		return new ResponseEntity<>(response, HttpStatus.OK);
    		
    	} catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    		
    	}
    	
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCliente(@RequestBody Cliente newPessoa, @PathVariable Long id) {
    	
    	try {
    		
    	    Cliente response = repository.findById(id)
						    		   .map(obj -> {
						    			  obj.setNome(newPessoa.getNome());
						    			  obj.setDocumento(newPessoa.getDocumento());
						    			  obj.setNomeMae(newPessoa.getNomeMae());
						    			  obj.setNomePai(newPessoa.getNomePai());
						    			  obj.setDt_nascimento(newPessoa.getDt_nascimento());
						    			  return repository.save(obj);
						    		   })
						    		   .orElseGet(()->{
						    			   return null;
						    		   });
    	    if (response != null) {
    	    	return new ResponseEntity<>(response, HttpStatus.OK);
    	    } else {
    	    	return new ResponseEntity<>("Cliente nao Encontrado. Solicitacao Cancelada", HttpStatus.BAD_REQUEST);
    	    }
    		
    	} catch (Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    		
    	}
      
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
    	
    	try {
    		Cliente p = repository.findById(id).get();
    		repository.delete(p);
    		return new ResponseEntity<>("Deleção da Entidade realizada Sucesso : " + p, HttpStatus.OK);
    	} catch(Exception e) {
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    	
    }

}