package br.com.teste.spring.repository.search;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import br.com.teste.spring.entity.Cliente;

public class ClienteSpecificationsBuilder {
	
    private final List<SearchCriteria> params;

    public ClienteSpecificationsBuilder() {
        params = new ArrayList<SearchCriteria>();
    }

    public ClienteSpecificationsBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Specification<Cliente> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification> specs = params.stream().map(ClienteSpecification::new).collect(Collectors.toList());
        
        Specification result = Specification.where(specs.get(0));

        for (int i = 1; i < params.size(); i++) {
            result  = result.and(specs.get(i));
        }
        
        return result;
    }
}
