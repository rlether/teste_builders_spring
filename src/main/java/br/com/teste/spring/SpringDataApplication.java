package br.com.teste.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

@SpringBootApplication
@EnableAutoConfiguration
public class SpringDataApplication {
	
    @Autowired
    private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(SpringDataApplication.class, args);
	}
	
    @PostConstruct
    private void initDb() {
        System.out.println("****** Inserindo dados DEMO na tabela : Pessoas ******");
        String sqlStatements[] = {
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Rodrigo', '11111111111', 'Mae 1', parsedatetime('17/09/2012', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Flavia', '22222222222', 'Mae 2', parsedatetime('01/01/1959', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Felipe', '33333333333', 'Mae 3', parsedatetime('01/01/2020', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Paloma', '44444444444', 'Mae 4', parsedatetime('01/01/2020', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Valter', '55555555555', 'Mae 5', parsedatetime('01/01/2020', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Gael', '66666666666', 'Mae 6', parsedatetime('01/01/2020', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Jhonatan', '77777777777', 'Mae 7', parsedatetime('01/01/2020', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Aline', '88888888888', 'Mae 8', parsedatetime('01/01/2020', 'dd/MM/yyyy'))",
            "insert into cliente(NOME, DOCUMENTO, NOME_MAE, DT_NASC) values('Miguel', '99999999999', 'Mae 9', parsedatetime('01/01/2020', 'dd/MM/yyyy'))"
        };

        Arrays.asList(sqlStatements).stream().forEach(sql -> {
            System.out.println(sql);
            jdbcTemplate.execute(sql);
        });

        System.out.println(String.format("****** SELECT from table: %s ******", "CLIENTE"));
        jdbcTemplate.query("select id_cliente,nome,documento from cliente",
            new RowMapper<Object>() {
                @Override
                public Object mapRow(ResultSet rs, int i) throws SQLException {
                    System.out.println(String.format("id_pessoa:%s,nome:%s,documento:%s",
                        rs.getString("id_cliente"),
                        rs.getString("nome"),
                        rs.getString("documento")));
                    return null;
                }
        });
    }

}